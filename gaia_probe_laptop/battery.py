

class Battery:

    def __init__(self, id):
        self.event_file_path = \
            '/sys/class/power_supply/BAT{}/power_now'.format(id)

    @property
    def watts(self):
        with open(self.event_file_path) as event_file:
            return int(event_file.read())
