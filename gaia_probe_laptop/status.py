import os

from .battery import Battery


class Status:

    @property
    def watts(self):
        return sum(
            Battery(n.replace('BAT', '')).watts
            for n in os.listdir('/sys/class/power_supply')
            if n.startswith('BAT')
        )
