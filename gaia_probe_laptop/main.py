import asyncio
import json
import sys

from .client import Client
from .status import Status


async def _do():
    client = Client(sys.argv[1])
    while True:
        data = await client.send(Status().watts)
        if data:
            sys.stdout.write('sent: {}\n'.format(
                json.dumps(data)
            ))
        await asyncio.sleep(1)


def cli():
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(_do())
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()
