import json
import sys
from uuid import uuid4

from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientConnectorError
from aiohttp.client_exceptions import ClientResponseError


class Client:

    def __init__(self, host, category=1, source=None):
        self.category = category
        self.host = host
        self.source = source or uuid4()

    async def send(self, watts):
        async with ClientSession(raise_for_status=True) as session:
            try:
                async with session.post(
                    self.host + '/uptakes',
                    data=json.dumps({
                        'category': self.category,
                        'unit': 'uw',
                        'source': str(self.source),
                        'value': watts,
                    })
                ) as response:
                    try:
                        return await response.json()
                    except ValueError:
                        sys.stderr.write('invalid response: %s', response)
            except ClientConnectorError:
                sys.stderr.write('Can connect to: {}\n'.format(self.host))
            except ClientResponseError as err:
                sys.stderr.write('Response error: {}\n'.format(err))
